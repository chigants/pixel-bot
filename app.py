#!/usr/bin/env python
#
#   Pixel bot
#

from email.headerregistry import MessageIDHeader
import os
import logging
import random

from zmq import Message
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters
from telegram import Update

##
#   Pixel bot settings
##
MESSAGE = 'Miau'    # Message the bot responds
CHANCE = 9          # Set probability for the bot to respond. Probability = 1 / (CHANCE + 1)

##
#   Logging configuration
##
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

##
#   Say MESSAGE
##
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text=MESSAGE)

##
#   Say MESSAGE in a set chance
##
async def miau(update: Update, context: ContextTypes.DEFAULT_TYPE):
    msg = MESSAGE
    num_ps = len(update.effective_message.text.split("ps")) - 1
    chance = CHANCE

    if num_ps > 1:
        chance = chance - num_ps
        msg = msg.upper()
    
    if chance <= 0:
        chance = 1

    r = random.randint(0, chance)
    if r == 0:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)

##
#   Main script
##
if __name__ == '__main__':
    # Read bot token from file API_TOKEN_FILE
    bot_token = open(os.environ['API_TOKEN_FILE'], mode='r').read()
    bot = ApplicationBuilder().token(bot_token).build()
    
    # Say MESSAGE upon /start
    start_handler = CommandHandler('start', start)
    bot.add_handler(start_handler)

    # Say MESSAGE upon new message received with a set chance
    convert_handler = MessageHandler(filters.TEXT, miau)
    bot.add_handler(convert_handler)

    bot.run_polling()