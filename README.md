Pixel Bot
=========

Este es el repositorio del bot de Pixel, un gato que responderá algunas veces a
los mensajes que se le envíe (ya sea en chats privados o grupales).


Despliegue
----------

Para **iniciar** la operativa del bot:

```
docker-compose up --build -d
```

Para **detener** la operativa del bot:

```
docker-compose down
```


Configuración
-------------

Se debe de proveer del TOKEN del Bot en un fichero tal y como se marca en el `docker-compose.yaml`, por defecto `bot_token`.

Se puede ajustar el **mensaje** (`MESSAGE`) y la **probabilidad** (`CHANCE`) de
respuesta del bot. Ambos se especifican en el apartado *"Pixel bot settings"*
del fichero `app.py`.